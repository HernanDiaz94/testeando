class Calculadora {
    constructor(valor1,valor2){
        this.valor1 = valor1;
        this.valor2 = valor2;
    };

    static suma(numero1,numero2){
        return numero1 + numero2;
    }
    static resta(numero1,numero2){
        return numero1 - numero2;
    }
    static multiplicacion(numero1,numero2){
        return numero1 * numero2;
    }
    static division(numero1,numero2){
        return numero1 / numero2;
    }
}

var numero1 = parseInt(prompt('Ingresa valor 1'));
var numero2 = parseInt(prompt('Ingresa valor 2'));

var menu ="Menu \n";
menu += "1. Suma\n";
menu += "2. Resta\n";
menu += "3. Multiplicacion\n";
menu += "4. Division\n";
menu += "5. Salir\n";
var opcion = 0;
var resultado=0;
var operacion = "";
do{
    opcion = parseInt(prompt(menu));
    
    // console.log(opcion);
    switch (opcion) {
        case 1:
            resultado = Calculadora.suma(numero1,numero2);
            operacion = "+";
            break;
        case 2:
            resultado= Calculadora.resta(numero1,numero2);
            operacion = "-";
            break;
        case 3:
            resultado= Calculadora.multiplicacion(numero1,numero2);
            operacion = "*";
            break;
        case 4:
            resultado= Calculadora.division(numero1,numero2);
            operacion = "/";
            break;
        case 5:
            break;        
        default: 
            console.log("Opcion incorrecta");
            break;
    }
    console.log("Valor 1 = "+numero1 + "," + "Valor 2 = "+numero2);
    console.log("Resultado: "+numero1 + " " + operacion + " " + numero2 + " = " + resultado);

}while (opcion!=5)



